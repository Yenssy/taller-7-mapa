// crear un objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa = L.map('mapid');

//Determinar la vista inicial
miMapa.setView([4.581780,-74.106750], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);


//Crear un objeto marcador
let miMarcador = L.marker([4.583997, -74.105531]);
miMarcador.addTo(miMapa);

miMarcador.bindPopup("<br>Soy la junta de acción comunal:).").openPopup();

//JSON
let polygon = L.polygon([
    [4.584414, -74.105317],
    [4.584243, -74.109474],
    [4.582195, -74.108365],
    [4.584179, -74.105113]

]).addTo(miMapa);

polygon.bindPopup("Soy el parque y ocupo un gran porcentaje del barrio :).");

var circle = L.circle([4.582072, -74.106889], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 320
}).addTo(miMapa);

circle.bindPopup("Soy el barrio Olaya :).");


